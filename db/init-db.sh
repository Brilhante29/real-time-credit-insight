#!/bin/bash
set -e

# Execute the SQL dump file
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" -f /docker-entrypoint-initdb.d/dump.sql

# Call the original entrypoint script
/docker-entrypoint.sh "$@"